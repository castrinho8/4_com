/*PRACTICA 1: ANALIZADOR ESTADÍSTICO DE TEXTOS*/


/*---Sección de declaraciones---*/

%{
int charCount = 0, wordCount = 0, upperCount = 0, lowerCount = 0, phraseCount =
0, paragraphCount = 0;
%}
upperdot [A-Z0-9]+"."+
lowerdot [a-z0-9]+"."+
upperdotline [A-Z0-9]+"."+\n
lowerdotline [a-z0-9]+"."+\n
upper [A-Z0-9]+[ \t\n]
lower [a-z0-9]+[ \t\n]
word [^. \t\n]+
phrase "."+
eoline \n{1,}

/*---Sección de reglas---*/
%% 
{upperdot} {wordCount++; charCount += yyleng; upperCount++; phraseCount++;}
{lowerdot} {wordCount++; charCount += yyleng; lowerCount++; phraseCount++;}
{upperdotline} {wordCount++; charCount += yyleng-1; upperCount++; phraseCount++;}
{lowerdotline} {wordCount++; charCount += yyleng-1; lowerCount++; phraseCount++;}
{upper} {wordCount++; charCount += yyleng-1; upperCount++;}
{lower} {wordCount++; charCount += yyleng-1; lowerCount++;}
{word} {wordCount++; charCount += yyleng;}
{phrase} {charCount++; phraseCount++;}
{eoline} {paragraphCount++;}

%% 
/*---Sección de código---*/

  int main() {
    yylex();
    printf("\nChar:%i \t\t\tWord:%i\nPalabras en minuscula:%i \tPalabras en mayuscula:%i\nFrases:%i \t\t\tParrafos:%i\n",charCount, wordCount, lowerCount, upperCount,  phraseCount, paragraphCount);                                                   
  }



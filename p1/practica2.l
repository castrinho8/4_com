/*PRACTICA 2: TRATAMIENTO DE UN LISTADO DE ALUMNOS DE UNA ASIGNATURA*/

/*---Sección de declaraciones---*/
%{

//Constantes para definir el campo que se ha leido
#define EOFILE 0
#define FIRST 10
#define SIG 20
#define YEAR 30
#define HEAD 40
#define DNI 50
#define NAME 60
#define GROUP 70
#define CALIF 80
#define EO_LINE 90


//Estructura que guarda uno de los alumnos del fichero
typedef struct persona_ persona;
struct persona_{
   int linea_persona;
   char * dni;
   char * nombre;
   char * grupo;
   char * nota;
};

char * materia; //Asignatura
int linea_materia = 1;
char * curso; //Año
int linea_curso = 2;
char * cabecera; //Cabecera
int linea_cabecera = 3;

//Campos para cada persona
char * dni;
char * nombre;
char * grupo;
char * nota;
int linea_persona;

//Linea actual
int line = 0;
%}
eoline \n
ano Año:" "[0-9]{4}"/"[0-9]{4}\n
cabecera NIF.*Nota\n

dni [0-9]{8}-[a-zA-Z]{1}
grupo \t[0-9]{1}[A-Z]{1}\t
nota "Suspenso"|"Aprobado"|"Notable"|"Sobresaliente"|"Matricula de Honor"
nombre \t[^0-9\n\t ]+" "[^0-9\n\t ]+", "[^0-9\n\t ]+

asignatura ^[^\t\n]+\n

/*---Sección de reglas---*/
%% 
{ano} {line++; linea_curso = line; curso = strndup(yytext+strlen("Año: "),yyleng-strlen("Año: \n")); return YEAR;}
{cabecera} {line++; linea_cabecera = line; cabecera = strndup(yytext,yyleng-strlen("\n")); return HEAD;}

{dni} {linea_persona = line; dni = strdup(yytext); return DNI;}
{nombre} {linea_persona = line; nombre = strndup(yytext+strlen("\t"),yyleng-strlen("\t")); return NAME;}
{grupo} {linea_persona = line; grupo = strndup(yytext+strlen("\t"),yyleng-strlen("\t\t")); return GROUP;}
{nota} {linea_persona = line; nota =  strdup(yytext); return CALIF;}

{eoline} {line++;return EO_LINE;}
{asignatura} {line++; linea_materia = line; materia = strndup(yytext,yyleng-strlen("\n"));return SIG;}
%% 
/*---Sección de código---*/

//Función que comprueba si hay algun NULL que representa un campo que no ha sido leido.
int hay_null(persona p){
		return((p.dni==NULL)||(p.nombre==NULL)||(p.grupo==NULL)||(p.nota==NULL));
}

//Función que comprueba si todos los campos son NULL.
int todos_null(persona p){
		return((p.dni==NULL)&&(p.nombre==NULL)&&(p.grupo==NULL)&&(p.nota==NULL));
}

//Función que selecciona aprobados y suspensos y los imprime
void imprime_lista(persona * lista,int tam_lista){
	persona * aprobados = (persona *) malloc(sizeof(persona));
	persona * suspensos = (persona *) malloc(sizeof(persona));
	
	int ind_sus=0,ind_apro=0;
	int j;

	for(j=0;j<tam_lista;j++){
		if(hay_null(lista[j])){//Si hay algun caso en el que haya habido un error nos lo saltamos
			continue;
		}if(!strcmp(lista[j].nota,"Suspenso")){
			suspensos[ind_sus] = lista[j];
			ind_sus++;
			suspensos = (persona *) realloc(suspensos,(ind_sus+1)*sizeof(persona));
		}else{
			aprobados[ind_apro] = lista[j];
			ind_apro++;
			aprobados = (persona *) realloc(aprobados,(ind_apro+1)*sizeof(persona));
		}
	}

	printf("Alumnos suspensos:\n");
	for(j=0;j<ind_sus;j++){
		printf("%s;%s\n",suspensos[j].dni,suspensos[j].nombre);
	}

	printf("Alumnos aprobados:\n");
	for(j=0;j<ind_apro;j++){
		printf("%s;%s;%s\n",aprobados[j].dni,aprobados[j].nombre,aprobados[j].nota);
	}
}

//Función que imprime los errores
void imprime_errores(persona * lista,int tam_lista){
	int j;
	
	printf("Errores:\n");
	
	if(materia == NULL)
		printf("Linea %i: No hay ASIGNATURA en el fichero o el formato es incorrecto.\n",linea_materia);      
	else	
		if(linea_materia != 1)
			printf("Linea %i: La ASIGNATURA se encuentra en un lugar incorrecto\n",linea_materia);   
		
	if(curso == NULL)
		printf("Linea %i: No hay AÑO en el fichero o el formato es incorrecto.\n",linea_curso);      
	else
		if(linea_curso != 2)
			printf("Linea %i: El AÑO se encuentra en un lugar incorrecto\n",linea_curso);
		
	if(cabecera == NULL)
		printf("Linea %i: No hay CABECERA en el fichero o el formato es incorrecto.\n",linea_cabecera);      
	else 
		if(linea_cabecera != 3)
			printf("Linea %i: La CABECERA se encuentra en un lugar incorrecto\n",linea_cabecera);      
		
	for(j=0;j<tam_lista;j++){
		if(todos_null(lista[j]))
			printf("Linea %i: Linea no reconocida.\n",lista[j].linea_persona);
		else{
			if(lista[j].dni == NULL)
				printf("Linea %i: No hay DNI en el fichero o el formato es incorrecto.\n",lista[j].linea_persona);      
			if(lista[j].nombre == NULL)
				printf("Linea %i: No hay NOMBRE en el fichero o el formato es incorrecto.\n",lista[j].linea_persona);   
			if(lista[j].grupo == NULL)
				printf("Linea %i: No hay GRUPO en el fichero o el formato es incorrecto.\n",lista[j].linea_persona);   
			if(lista[j].nota == NULL)
				printf("Linea %i: No hay NOTA en el fichero o el formato es incorrecto.\n",lista[j].linea_persona);
		}
	}
}

int main() {
	int type,pos = 0,pos_er = 0,control = FIRST;
	persona * lista = (persona *) calloc(1,sizeof(persona));
	lista[0].dni = NULL; lista[0].nombre = NULL; lista[0].grupo = NULL; lista[0].nota = NULL; 
	
	while((type = yylex()) != EOFILE){
		switch (type){
				case HEAD:{
					control = HEAD;
					break;
				}
				case DNI:{
					control = DNI;
					lista[pos].dni = dni;
					break;
				}
				case NAME:{
					control = NAME;
					lista[pos].nombre = nombre;
					break;
				}
				case GROUP:{
					control = GROUP;
					lista[pos].grupo = grupo;
					break;
				}
				case CALIF:{
					control = CALIF;
					lista[pos].nota = nota;
					break;
				}
				case EO_LINE:{
					if(control >= HEAD){
						lista[pos].linea_persona = line;
						pos++;
						lista = (persona *) realloc(lista,(pos+1)*sizeof(persona));
						lista[pos].dni = NULL; lista[pos].nombre = NULL; lista[pos].grupo = NULL; lista[pos].nota = NULL; 
					}
					break;
				}
			}//Switch
	}//While
	
	printf("\n\nAsignatura: %s\nCurso: %s\n",materia,curso);
	imprime_lista(lista,pos);
	imprime_errores(lista,pos);
}


  
  
  
  
  
  
  
  

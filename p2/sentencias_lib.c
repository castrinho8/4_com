#include <stdio.h> 
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <aio.h>
#include <stdlib.h>

//Números máximos de operaciones y campos permitidos en la sentencia.
#define MAX_OPERACIONES 20
#define MAX_CAMPOS 20

typedef enum {T_MENOR, T_IGUAL, T_MAYOR, T_MENOR_IGUAL, T_MAYOR_IGUAL, T_DIFERENTE} t_operador;
typedef enum {T_ASTERISCO, T_ID, T_NOMBRE, T_PUESTO, T_ANO, T_SALARIO} t_campos;
typedef enum {T_AND,T_OR,T_EMPTY} t_and_or;

//Estructura que guarda una operacion
typedef struct operacion_ operacion;

struct operacion_{
		char * operando1;
		char * operador;
		char * operando2;
		t_and_or and_or;
};

struct sentencia_{
		int n_campos;
		char * lista_campos[MAX_CAMPOS];
		char * tabla;
		int n_operaciones;
		operacion * lista_operaciones[MAX_OPERACIONES];
};

//Sentencia que contiene los datos con los que operar.
struct sentencia_ * sentencia;
int last = 0;

//CABECERAS DE LAS FUNCIONES DEFINIDAS EN ESTE MÓDULO
void inicializa_sentencia();
int addCampo(char * campo);
void addTabla(char * tabla);
int addOperacion(char * op1, char * operador, char * op2,char * logic);
t_operador charTOoperador(char * operador);
char * operadorTOchar(t_operador op);
t_and_or charTOAnd_or(char * logic_operator);
char * and_orTOchar(t_and_or logic_operator);
t_campos charTOcampos(char * campo);
char * camposTOchar(t_campos campo);


/*
 * Inicializa sentencia y todos sus campos y operaciones.
 */
void inicializa_sentencia(){
	int i = 0;
	sentencia = (struct sentencia_ *) malloc(sizeof(struct sentencia_));
	
	sentencia->n_campos = 0;
	sentencia->n_operaciones = 0;
	sentencia->tabla = strdup("\0");
	
	for(i=0;i<MAX_CAMPOS;i++)
		sentencia->lista_campos[i] = strdup("\0");

	for(i=0;i<MAX_OPERACIONES;i++){
		sentencia->lista_operaciones[i] = (operacion *) malloc(sizeof(operacion));
		sentencia->lista_operaciones[i]->operando1 = strdup("\0");
		sentencia->lista_operaciones[i]->operador = strdup("\0");
		sentencia->lista_operaciones[i]->operando2 = strdup("\0");
		sentencia->lista_operaciones[i]->and_or = T_EMPTY;
	}
}

/*
 * Añade un campo a la sentencia y devuelve la ultima posición en la que se encuentra el array de campos
 * ERROR: devuelve -1
*/
int addCampo(char * campo){
	if(sentencia->n_campos+1 == MAX_CAMPOS){
		printf("ERROR: Alcanzado el número máximo de campos permitidos en el SELECT.\n");
		return -1;
	}
	sentencia->lista_campos[sentencia->n_campos] = strdup(campo);
	sentencia->n_campos++;
	return sentencia->n_campos;	
}

/*
 * Añade una tabla a la sentencia
*/
void addTabla(char * tabla){
	sentencia->tabla = strdup(tabla);
}

/*
 * Añade una operación a la sentencia y devuelve la ultima posición en la que se encuentra el array de operaciones
 * ERROR: devuelve -1
*/
int addOperacion(char * op1, char * operador, char * op2,char * logic){
	if(sentencia->n_operaciones+1 == MAX_OPERACIONES){
		printf("ERROR: Alcanzado el número máximo de operaciones permitidas en el WHERE.\n");
		return -1;
	}
	sentencia->lista_operaciones[sentencia->n_operaciones]->and_or = charTOAnd_or(logic);
	
	sentencia->lista_operaciones[sentencia->n_operaciones]->operando1 = strdup(op1);
	sentencia->lista_operaciones[sentencia->n_operaciones]->operador = strdup(operador);
	sentencia->lista_operaciones[sentencia->n_operaciones]->operando2 = strdup(op2);
	sentencia->n_operaciones++;
	return sentencia->n_operaciones;	
}

/*
 * Funcion que recibe un operador en char * y devuelve el t_operador correspondiente 
*/
t_operador charTOoperador(char * operador){
		
	if(!strcmp(operador,"<"))
		return T_MENOR;
	if(!strcmp(operador,"="))
		return T_IGUAL;
	if(!strcmp(operador,">"))
		return T_MAYOR;
	if(!strcmp(operador,"<="))
		return T_MENOR_IGUAL;
	if(!strcmp(operador,">="))
		return T_MAYOR_IGUAL;
	if(!strcmp(operador,"!="))
		return T_DIFERENTE;
}

/*
 * Funcion que recibe un operador en t_operador y devueove el char * correspondiente
*/
char * operadorTOchar(t_operador op){
	
		switch(op){
			case T_MENOR :{
				return "<";
			}
			case T_IGUAL :{
				return "=";
			}
			case T_MAYOR :{
				return ">";
			}
			case T_MENOR_IGUAL :{
				return ">=";
			}
			case T_MAYOR_IGUAL :{
				return ">=";
			}		
			case T_DIFERENTE :{
				return "!=";
			}				
		}
}

/*
 * Función que recibe un AND o OR en char * y devuelve el t_operador correspondiente
*/
t_and_or charTOAnd_or(char * logic_operator){
		if(!strcmp(logic_operator,"EMPTY")){
			return T_EMPTY;
		}
		if(!strcmp(logic_operator,"AND")){
			return T_AND;
		}
		if(!strcmp(logic_operator,"OR")){
			return T_OR;
		}
		return T_EMPTY;
}

/*
 * Función que recibe un t_and_or y devuelve el char * correspondiente
 */
char * and_orTOchar(t_and_or logic_operator){
	switch(logic_operator){
		case T_EMPTY :{
			return "EMPTY";
		}
		case T_AND :{
			return "AND";
		}
		case T_OR :{
			return "OR";
		}
	}
}

/*
 * Función que recibe un campo en char * y devuelve el t_campos correspondiente
*/
t_campos charTOcampos(char * campo){
	
	if(!strcmp(campo,"*")){
		return T_ASTERISCO;
	}
	if(!strcmp(campo,"idEmpleado")){
		return T_ID;
	}
	if(!strcmp(campo,"nombre")){
		return T_NOMBRE;
	}
	if(!strcmp(campo,"puesto")){
		return T_PUESTO;
	}
	if(!strcmp(campo,"anho")){
		return T_ANO;
	}
	if(!strcmp(campo,"salario")){
		return T_SALARIO;
	}
	
}

/*
 * Función que recibe un campo en t_campos y devuelve el char * correspondiente
*/
char * camposTOchar(t_campos campo){

	switch(campo){
		case T_ASTERISCO:{
			return "*";
		}
		case T_ID:{
			return "idEmpleado";
		}
		case T_NOMBRE:{
			return "nombre";
		}
		case T_PUESTO:{
			return "puesto";
		}
		case T_ANO:{
			return "anho";
		}
		case T_SALARIO:{
			return "salario";
		}
	}
}



/*
 * Función que muestra la sentencia introducida.
*/ 
void imprime_sentencia(){
	printf("\n-SENTENCIA-\n");
	int i = 0;
	printf("SELECT ");
	for(i=0;i<sentencia->n_campos;i++){
		if(i == sentencia->n_campos-1)
			printf("%s",sentencia->lista_campos[i]);
		else
			printf("%s,",sentencia->lista_campos[i]);
	}printf(" FROM %s\nWHERE ",sentencia->tabla);
	for(i=0;i<sentencia->n_operaciones;i++){
		printf("%s%s%s ",sentencia->lista_operaciones[i]->operando1,sentencia->lista_operaciones[i]->operador,sentencia->lista_operaciones[i]->operando2);
		if(sentencia->lista_operaciones[i]->and_or != T_EMPTY)
			printf("%s ",and_orTOchar(sentencia->lista_operaciones[i]->and_or));
	}
	printf("\n\n");
}











/*PRACTICA 2: BISON*/
%error-verbose
%{
#include <stdio.h>
#include "basedatos_lib.c"

//Constantes que controlan que fichero se está leyendo
#define READ_EMPLEADOS 30
#define READ_SENTENCE 31

//EMPLEADOS
int linea_empleados = 1; //Linea en la que nos encontramos

//SQL
int linea_sentencia = 1; //Linea en la que nos encontramos

//Contiene los resultados de aplicar las diferentes operaciones, cada una en una columna.
int * salida_operaciones[MAX_OPERACIONES];
int ultimo_salida_operaciones = 0; //Tamaño actual del array anterior
int * resultado_final;

int current_action; //Indica que fichero se esta leyendo


//Función que imprime el error indicado y detiene el programa.
void yyerror (const char *s) {
	switch (current_action){
		case READ_EMPLEADOS:{
			fprintf (stderr, "Linea empleados:%i - %s\n",linea_empleados,s);
			break;
			}
		case READ_SENTENCE:{
			fprintf (stderr, "-%s-\n",s);
			break;
			}
	}
	exit(-1);
}

%}
//variable global
%union {
	//EMPLEADOS
	long id;
	char * name_job;
	char * year;
	double salary;
	
	//SQL
	char * campo;
	char * tabla;
	char * operando_campo;
	char * operador;
	char * operando_valor;
	char * and_or;
	char * coma;
	int fin;
	char * valerror;
	
	int valint;
	char * valchar;
}


//EMPLEADOS
%token <id> ID
%token <name_job> NAME_JOB
%token <year> YEAR
%token <salary> SALARY

//SQL
%token <campo> CAMPO
%token <campo> ASTERISCO
%token <tabla> TABLA
%token <operando_campo> OPERANDO_CAMPO
%token <operando_valor> OPERANDO_VALOR
%token <operador> OPERADOR
%token <and_or> AND
%token <and_or> OR
%token <coma> COMA
%token <fin> FIN
%token <valerror> ERROR
			
%type <valint> select
%type <valchar> from
%type <valchar> valor_logico

//simbolo inicial
%start S


%%

S :	sentencia
		| lista_trabajadores

		
lista_trabajadores : lista_trabajadores trabajador	{}
						| trabajador	{}

trabajador : ID NAME_JOB NAME_JOB YEAR SALARY {
																linea_empleados++;
																insertar_empleado($1,$2,$3,$4,$5);
																}



sentencia : 	select from where {	imprime_sentencia();
												resultado_final = compara_condiciones(salida_operaciones,ultimo_salida_operaciones);
												imprime_coincidencias(resultado_final);
												}
	
select : lista_campos {}
			|ASTERISCO  {addCampo($1);}
			
lista_campos :	lista_campos CAMPO {addCampo($2);}
					| CAMPO {addCampo($1);}

from :	TABLA { addTabla($1);}
			
where	:	where operaciones  {}
			|operaciones {}

operaciones	:	OPERANDO_CAMPO OPERADOR OPERANDO_VALOR valor_logico {
					addOperacion($1,$2,$3,$4);
					salida_operaciones[ultimo_salida_operaciones] = comprueba_tabla(1,0,$1,$2,$3);
					ultimo_salida_operaciones++;
					}
					|OPERANDO_VALOR OPERADOR OPERANDO_CAMPO valor_logico {
					addOperacion($1,$2,$3,$4);
					salida_operaciones[ultimo_salida_operaciones] = comprueba_tabla(0,1,$1,$2,$3);
					ultimo_salida_operaciones++;
					}
					|OPERANDO_CAMPO OPERADOR OPERANDO_CAMPO valor_logico {
					addOperacion($1,$2,$3,$4);
					salida_operaciones[ultimo_salida_operaciones] = comprueba_tabla(1,1,$1,$2,$3);
					ultimo_salida_operaciones++;
					}
					|OPERANDO_CAMPO OPERADOR OPERANDO_VALOR FIN {
					addOperacion($1,$2,$3,"EMPTY");
					salida_operaciones[ultimo_salida_operaciones] = comprueba_tabla(1,0,$1,$2,$3);
					ultimo_salida_operaciones++;
					linea_sentencia = $4;
					}
					|OPERANDO_VALOR OPERADOR OPERANDO_CAMPO FIN {
					addOperacion($1,$2,$3,"EMPTY");
					salida_operaciones[ultimo_salida_operaciones] = comprueba_tabla(0,1,$1,$2,$3);
					ultimo_salida_operaciones++;
					linea_sentencia = $4;
					}
					|OPERANDO_CAMPO OPERADOR OPERANDO_CAMPO FIN {
					addOperacion($1,$2,$3,"EMPTY");
					salida_operaciones[ultimo_salida_operaciones] = comprueba_tabla(1,1,$1,$2,$3);
					ultimo_salida_operaciones++;
					linea_sentencia = $4;
					}

valor_logico :	AND {$$ = "AND";}
					| OR {$$ = "OR";}


%%
void inicializa_global_vars(){

	linea_sentencia = 1; //Linea en la que nos encontramos
	ultimo_salida_operaciones = 0; //Tamaño actual del array anterior
	
	int i = 0, j = 0;
	//Reserva memoria para las variables globales
	for(i=0;i<MAX_OPERACIONES;i++){
		salida_operaciones[i] = malloc(tam_lista_empleados*sizeof(int));
		for(j=0;j<tam_lista_empleados;j++)
			salida_operaciones[i][j] = 0;
	}
	resultado_final = malloc(tam_lista_empleados*sizeof(int));
	inicializa_sentencia();
	
}

void ejecuta_fichero_prueba(char * fichero_lectura){

	int i = 0;
	char * fichero = malloc(50*sizeof(char));
	sprintf(fichero,"./doc/%s",fichero_lectura); //Concatena el directorio

	current_action = READ_SENTENCE;
	abrir_fichero(fichero);
	inicializa_global_vars();

	yyparse();
	
	free(fichero);
}

void ejecuta_tests_propuestos(int control){

	int i = 0, j = 5;
	char * fichero = malloc(50*sizeof(char));
		
	switch(control){
		case 1 :{
			for(j=0;j<=5;j++){
				current_action = READ_SENTENCE;
				sprintf(fichero,"./doc/sentencia%i.txt",j);
				
				abrir_fichero(fichero);
				inicializa_global_vars();
				
				yyparse();
			}
			break;
		}
		case 2:{
			current_action = READ_SENTENCE;
			sprintf(fichero,"./doc/sentencia_error_select.txt");
				
			abrir_fichero(fichero);
			printf("----SENTENCIA----\nSELECT idEmpleado,campo_invalido,puesto,anho FROM Empleado\nWHERE puesto=Programador AND puesto=Programadora;\n");
			inicializa_global_vars();
				
			yyparse();
			break;
		}
		case 3:{
			current_action = READ_SENTENCE;
			sprintf(fichero,"./doc/sentencia_error_from.txt");
				
			abrir_fichero(fichero);
			printf("\n----SENTENCIA----\nSELECT idEmpleado,nombre,puesto,anho FROM no_existe\nWHERE puesto=Programador AND puesto=Programadora;\n");
			inicializa_global_vars();
				
			yyparse();
			break;
		}
		case 4:{
			current_action = READ_SENTENCE;
			sprintf(fichero,"./doc/sentencia_error_where.txt");
				
			abrir_fichero(fichero);
			printf("\n----SENTENCIA----\nSELECT idEmpleado,nombre,puesto,anho FROM Empleado\nWHERE puesto=Programador puesto=Programadora;\n");
			inicializa_global_vars();
				
			yyparse();
			break;		
		}
	}
	free(fichero);
}

main() {
	int i = 0, control = 0, control2 = 0;
	char * fichero = malloc(50*sizeof(char));
	char * fichero_lectura = malloc(50*sizeof(char));

	current_action = READ_EMPLEADOS;
	linea_empleados = 1;
	
	sprintf(fichero,"./doc/Empleado.txt");
	abrir_fichero(fichero);
	lista_empleados = malloc(sizeof(empleado));
	
	yyparse();

	//Escoje el metodo de ejecucion.
	while ((control != 1) && (control != 2)){
		printf("\nSeleccione el tipo de ejecución\n\n1-Ejecución con los tests propuestos\n2-Ejecución con un test nuevo añadido\n");
		scanf("%i",&control);
	}
	
	if (control == 2){
		printf("Introduzca nombre del fichero de prueba añadido en la carpeta\n");
		scanf("%s",fichero_lectura);
		ejecuta_fichero_prueba(fichero_lectura);
	}
	else if(control == 1){
		while((control2 != 1) && (control2 != 2) && (control2 != 3) && (control2 != 4)){
			printf("1-Si desea ejecutar los casos de test habituales.\n2-Fichero de test para el error en el SELECT.\n3-Fichero de test para el error en el FROM.\n4-Fichero de test para el error en el WHERE.\n");
			scanf("%i",&control2);
		}
		ejecuta_tests_propuestos(control2);
	}
	
	
	free(fichero);
	free(fichero_lectura);
}


















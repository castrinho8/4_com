/*PRACTICA 2: lex*/
%{
#include "practica2.tab.h"

char * sal;
int linea = 1;
%}

%Start read_employee
emp_year [0-9]{4}
emp_salary [0-9\.]+\n
emp_id [0-9]+
emp_name_job [^\.,\t\n0-9]+
coma ,


%Start read_sentence
%Start has_select
%Start has_campos
%Start has_asterisco
%Start has_from
%Start has_table
%Start ready_operation

separador [ \t\n]+
new_line \n

select SELECT{separador}

asterisco \*
id idEmpleado
name nombre
job puesto
year anho
salary salario
end ;

from {separador}FROM{separador}
tabla Empleado

where {separador}WHERE{separador}|{new_line}WHERE{separador}
and {separador}AND{separador}
or {separador}OR{separador}
operando_campo {id}|{name}|{job}|{year}|{salary}
operando_valor [^ ,;.=<>!\n\t]+
operador <|<=|>|>=|!=|=
error .


/*---Sección de reglas---*/
%%
<read_employee>{coma} {}
<read_employee>{new_line} {linea++;}
<read_employee>{emp_year}	{yylval.year = strdup(yytext);
									return YEAR;}
<read_employee>{emp_id}			{yylval.id = atoi(yytext);
										return ID;}
<read_employee>{emp_name_job}	{yylval.name_job = strdup(yytext);
										return NAME_JOB;}
<read_employee>{emp_salary}	{sal = strndup(yytext,strlen(yytext-1));
										linea++;
										yylval.salary= atof(sal);
										return SALARY;}



{new_line} {linea++;}

<read_sentence>{select}	{BEGIN has_select;}

<read_sentence>{error} {
								fprintf(stderr,"\nError SELECT: Falta SELECT.\n");
								return ERROR;}

<has_select>{
	{asterisco}	{BEGIN has_asterisco;
					yylval.campo = strdup(yytext);
					return ASTERISCO;}
	{id}			{BEGIN has_campos;
					yylval.campo = strdup(yytext);
					return CAMPO;}
	{name}		{BEGIN has_campos;
					yylval.campo = strdup(yytext);
					return CAMPO;}
	{job}			{BEGIN has_campos;
					yylval.campo = strdup(yytext);
					return CAMPO;}
	{year}		{BEGIN has_campos;
					yylval.campo = strdup(yytext);
					return CAMPO;}
	{salary}		{BEGIN has_campos;
					yylval.campo = strdup(yytext);
					return CAMPO;}
	{error}		{
					fprintf(stderr,"\nError SELECT: Campo especificado incorrecto.\n");
					return ERROR;}
}

<has_campos>{coma} {BEGIN has_select;}

<has_campos,has_asterisco>{from}	{BEGIN has_from;
												}
												
<has_campos,has_asterisco>{error} {
												fprintf(stderr,"\nError FROM: Falta FROM.\n");
												return ERROR;}

<has_from>{tabla}		{BEGIN has_table;
							yylval.tabla = strdup(yytext);
							return TABLA;}

<has_from>{error} 	{
										fprintf(stderr,"\nError FROM: Tabla incorrecta.\n");
										return ERROR;}

<has_table>{where}	{BEGIN ready_operation;
							}

<has_table>{error} 	{
										fprintf(stderr,"\nError WHERE: Falta WHERE.\n");
										return ERROR;}

<ready_operation>{operando_campo}	{
												yylval.operando_campo = strdup(yytext);
												return OPERANDO_CAMPO;
												}

<ready_operation>{operando_valor}	{
												yylval.operando_valor = strdup(yytext);
												return OPERANDO_VALOR;
												}

<ready_operation>{operador}		{
											yylval.operador = strdup(yytext);
											return OPERADOR;
											}

<ready_operation>{and}		{
										yylval.and_or = strdup(yytext);
										return AND;
										}

<ready_operation>{or}		{
										yylval.and_or = strdup(yytext);
										return OR;
										}
										

{end}		{
			yylval.fin = linea;
			return FIN;}
			
<ready_operation>{error} {		
										fprintf(stderr,"\nError WHERE: Campo incorrecto\n");
										return ERROR;}
										
%% 

//Abre el fichero indicado y cambia de buffer
void abrir_fichero(char * path){
	
	linea = 0;

	if(!strcmp(path,"./doc/Empleado.txt"))
		BEGIN read_employee;
	else
		BEGIN read_sentence;
		
	FILE * fichero;
	YY_BUFFER_STATE buffer1, buffer2;
	
	if((fichero = fopen(path,"r")) == NULL){
		printf("Error: El fichero introducido no existe.\nAbortada la ejecución\n");
		exit(-1);
	}
   buffer1 = yy_create_buffer (fichero,YY_BUF_SIZE);

	yy_switch_to_buffer (buffer1);
}


















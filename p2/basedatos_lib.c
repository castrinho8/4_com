#include <stdio.h> 
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <aio.h>
#include <stdlib.h>

#include "sentencias_lib.c"

//Estructura que guarda uno de los empleados
typedef struct empleado_ empleado;

struct empleado_{
   long idEmpleado; //not null
   char * nombre; //not null
   char * puesto; //not null
   char * anho; //not null
   double salario; //not null
};

//Variables que contienen a todos los empleados
empleado * lista_empleados;
int tam_lista_empleados = 0;


//CABECERAS DE LAS FUNCIONES DEFINIDAS EN ESTE MÓDULO
void insertar_empleado(long i,char * n,char * j, char * y,double s);
int * comprueba_campo_valor(char * campo, t_operador operador, char * valor);
int * comprueba_dos_campos(char * campo1, t_operador operador, char * campo2);
int compara_doubles(double a, t_operador operador, double b);
int compara_double_int(double a, t_operador operador, int b);
int compara_int_double(int a, t_operador operador, double b);
int compara_chars(char * a, t_operador operador, char * b);
int * comprueba_tabla(int ind_campo1, int ind_campo2,char * val1, char * operador, char * val2);
int * compara_condiciones(int ** lista_salidas,int tam_lista_salidas);
void imprime_cabecera_tabla(int string_mas_largo);
void imprime_coincidencias(int * lista);
	

/*
 * Crea un empleado y lo inserta 
*/
void insertar_empleado(long i,char * n,char * j, char * y,double s){
	lista_empleados[tam_lista_empleados].idEmpleado = i;
	lista_empleados[tam_lista_empleados].nombre = strdup(n);
	lista_empleados[tam_lista_empleados].puesto = strdup(j);
	lista_empleados[tam_lista_empleados].anho = strdup(y);
	lista_empleados[tam_lista_empleados].salario = s;
		
	tam_lista_empleados++;
	lista_empleados = (empleado *) realloc(lista_empleados,(tam_lista_empleados+1)*sizeof(empleado));
}


/*
 * Funcion que recibe un operando_campo, un t_operador y un operando_valor y devuelve un array marcado con 1 en las posiciones en las que se cumple la condicion.
 */
int * comprueba_campo_valor(char * campo, t_operador operador, char * valor){
	
	int i = 0;
	int * posiciones = malloc(tam_lista_empleados*sizeof(int));

	for(i=0;i<tam_lista_empleados;i++)
		posiciones[i] = 0;
	
	if(!strcmp(campo,"idEmpleado")){
		for(i=0;i<tam_lista_empleados;i++){
			posiciones[i] = compara_int_double(lista_empleados[i].idEmpleado,operador,atof(valor));
		}
		return posiciones;
	}

	if(!strcmp(campo,"nombre")){
		for(i=0;i<tam_lista_empleados;i++){
			posiciones[i] = compara_chars(lista_empleados[i].nombre,operador,valor);
		}
		return posiciones;
	}
	
	if(!strcmp(campo,"puesto")){
		for(i=0;i<tam_lista_empleados;i++){
			posiciones[i] = compara_chars(lista_empleados[i].puesto,operador,valor);
		}
		return posiciones;
	}
	
	if(!strcmp(campo,"anho")){
		for(i=0;i<tam_lista_empleados;i++){
			posiciones[i] = compara_chars(lista_empleados[i].anho,operador,valor);
		}
		return posiciones;
	}
	
	if(!strcmp(campo,"salario")){
		for(i=0;i<tam_lista_empleados;i++){
			posiciones[i] = compara_doubles(lista_empleados[i].salario,operador,atof(valor));
		}
		return posiciones;
	}
}


/*
 * Funcion que recibe dos operando_campo y un t_operador y devuelve un array marcado con 1 en las posiciones en las que se cumple la condicion.
*/
int * comprueba_dos_campos(char * campo1, t_operador operador, char * campo2){

	int i = 0;
	int * posiciones = malloc(tam_lista_empleados*sizeof(int));
	
	for(i=0;i<tam_lista_empleados;i++)
		posiciones[i] = 0;
	
	//idEmpleado
	if(!strcmp(campo1,"idEmpleado")){
		if((!strcmp(campo2,"nombre")) || (!strcmp(campo2,"puesto")) || (!strcmp(campo2,"anho"))){
			printf("Error de tipos: no se puede comparar int y char *");
			exit(-1);
		}
		if(!strcmp(campo2,"salario")){
			for(i=0;i<tam_lista_empleados;i++){
				posiciones[i] = compara_int_double(lista_empleados[i].idEmpleado,operador,lista_empleados[i].salario);
			}
		}
	}

	//nombre
	if(!strcmp(campo1,"nombre")){
		if((!strcmp(campo2,"idEmpleado")) || (!strcmp(campo2,"salario"))){
			printf("Error de tipos: no se puede comparar int y char *");
			exit(-1);
		}
		if(!strcmp(campo2,"puesto")){
			for(i=0;i<tam_lista_empleados;i++){
				posiciones[i] = compara_chars(lista_empleados[i].nombre,operador,lista_empleados[i].anho);
			}
		}
		if(!strcmp(campo2,"anho")){
			for(i=0;i<tam_lista_empleados;i++){
				posiciones[i] = compara_chars(lista_empleados[i].nombre,operador,lista_empleados[i].anho);
			}
		}
	}
		
	//puesto
	if(!strcmp(campo1,"puesto")){
		if((!strcmp(campo2,"idEmpleado")) || (!strcmp(campo2,"salario"))){
			printf("Error de tipos: no se puede comparar int y char *");
			exit(-1);
		}
		if(!strcmp(campo2,"nombre")){
			for(i=0;i<tam_lista_empleados;i++){
				posiciones[i] = compara_chars(lista_empleados[i].puesto,operador,lista_empleados[i].nombre);
			}
		}
		if(!strcmp(campo2,"anho")){
			for(i=0;i<tam_lista_empleados;i++){
				posiciones[i] = compara_chars(lista_empleados[i].puesto,operador,lista_empleados[i].anho);
			}
		}
	}	
	
	//anho
	if(!strcmp(campo1,"anho")){
		if((!strcmp(campo2,"idEmpleado")) || (!strcmp(campo2,"salario"))){
			printf("Error de tipos: no se puede comparar int y char *");
			exit(-1);
		}
		if(!strcmp(campo2,"puesto")){
			for(i=0;i<tam_lista_empleados;i++){
				posiciones[i] = compara_chars(lista_empleados[i].anho,operador,lista_empleados[i].puesto);
			}
		}
		if(!strcmp(campo2,"nombre")){
			for(i=0;i<tam_lista_empleados;i++){
				posiciones[i] = compara_chars(lista_empleados[i].anho,operador,lista_empleados[i].nombre);
			}
		}
	}		
		
	//salario
	if(!strcmp(campo1,"salario")){
		if((!strcmp(campo2,"anho")) ||(!strcmp(campo2,"puesto")) || (!strcmp(campo2,"nombre"))){
			printf("Error de tipos: no se puede comparar int y char *");
			exit(-1);
		}		
		
		if(!strcmp(campo2,"idEmpleado")){
			for(i=0;i<tam_lista_empleados;i++){
				posiciones[i] = compara_double_int(lista_empleados[i].salario,operador,lista_empleados[i].idEmpleado);
			}
		}
	}
		for(i=0;i<tam_lista_empleados;i++)
		printf("---%i\n",posiciones[i]);
	return posiciones;
}


/*
 * Compara 2 double con el operador indicado
 */
int compara_doubles(double a, t_operador operador, double b){
	
	int resultado = 0;

	switch(operador){
		case T_MENOR:{
			if(a < b){
				resultado = 1;
			}else{
				resultado = 0;
			}break;
		}
		case T_IGUAL:{
			if(a == b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_MAYOR:{
			if(a > b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_MENOR_IGUAL:{
			if(a <= b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_MAYOR_IGUAL:{
			if(a >= b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_DIFERENTE:{
			if(a != b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
	}

	return resultado;
}


/*
 * Compara double e int con el operador indicado
 */
int compara_double_int(double a, t_operador operador, int b){
	
	int resultado = 0;

	switch(operador){
		case T_MENOR:{
			if(a < b){
				resultado = 1;
			}else{
				resultado = 0;
			}break;
		}
		case T_IGUAL:{
			if(a == b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_MAYOR:{
			if(a > b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_MENOR_IGUAL:{
			if(a <= b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_MAYOR_IGUAL:{
			if(a >= b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_DIFERENTE:{
			if(a != b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
	}
	return resultado;
}


/*
 * Compara int y double con el operador indicado
 */
int compara_int_double(int a, t_operador operador, double b){
	
	int resultado = 0;

	switch(operador){
		case T_MENOR:{
			if(a < b){
				resultado = 1;
			}else{
				resultado = 0;
			}break;
		}
		case T_IGUAL:{
			if(a == b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_MAYOR:{
			if(a > b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_MENOR_IGUAL:{
			if(a <= b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_MAYOR_IGUAL:{
			if(a >= b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
		case T_DIFERENTE:{
			if(a != b){
				resultado = 1;
			}else{
				resultado = 0;					
			}break;
		}
	}
	return resultado;
}


/*
 * Compara dos chars* con el operador indicado
 */
int compara_chars(char * a, t_operador operador, char * b){
	
	int resultado = 0;
	
	switch(operador){
		case T_MENOR:{
			if(strcmp(a,b) < 0){
				resultado = 1;
			}else{
				resultado = 0;
			}break;
		}
		case T_IGUAL:{
			if(strcmp(a,b) == 0){
				resultado = 1;
			}else{
				resultado = 0;				
			}break;
		}
		case T_MAYOR:{
			if(strcmp(a,b) > 0){
				resultado = 1;
			}else{
				resultado = 0;		
			}break;
		}
		case T_MENOR_IGUAL:{
			if(strcmp(a,b) <= 0){
				resultado = 1;
			}else{
				resultado = 0;		
			}break;
		}
		case T_MAYOR_IGUAL:{
			if(strcmp(a,b) >= 0){
				resultado = 1;
			}else{
				resultado = 0;		
			}break;
		}
		case T_DIFERENTE:{
			if(strcmp(a,b)){
				resultado = 1;
			}else{
				resultado = 0;
			}break;
		}
	}
	return resultado;
}


/*
 * Función que devuelve un array de ints con el resultado de la comparación del campo 
 + ind_campo1, ind_campo2 : variables booleanas que indican si son operando_campo o no.
 + val1, val2 = valor del campo
 + operador = operador;
*/
int * comprueba_tabla(int ind_campo1, int ind_campo2,char * val1, char * operador, char * val2){

	int i = 0;
	int * resultado = malloc(tam_lista_empleados*sizeof(int));

	for(i=0;i<tam_lista_empleados;i++)
		resultado[i] = 0;
	
	t_operador op = charTOoperador(operador);
	
	//Si los dos son operando_campo
	if(ind_campo1 && ind_campo2){
			if(!strcmp(val1,val2)){ //Si se compara el mismo campo devolvemos true
				for(i=0;i<tam_lista_empleados;i++)
					resultado[i] = 1;
			}else //Si los campos a comparar son distintos.
				resultado = comprueba_dos_campos(val1,op,val2);
		return resultado;
	}
	
	//Si el primer operando es el campo.
	if(ind_campo1){
		resultado = comprueba_campo_valor(val1, op, val2);
		return resultado;
	}
	
	//Si el segundo operando es el campo
	if(ind_campo2){
		resultado = comprueba_campo_valor(val2, op, val1);
		return resultado;
	}
	
}


/*
 * Función que realiza un AND o un OR segun indique la operación.
 * Recibe una matriz que contiene una columna por cada una de las condiciones del where y 
 * en cada una de ellas las salidas correspondientes a esa condición.
*/
int * compara_condiciones(int ** lista_salidas,int tam_lista_salidas){

	int i = 0,j = 0,k=0;
	int * resultado = malloc(tam_lista_empleados*sizeof(int));
	
	for(j=0;j<tam_lista_empleados;j++) //Inicializamos el resultado al resultado de la primera condición
		resultado[j] =  lista_salidas[0][j];
	
	for(i=0;i<tam_lista_salidas;i++){
		switch (sentencia->lista_operaciones[i]->and_or){
			case T_EMPTY:{ //Si no hay que hacer ninguna operación, se devuelve el resultado
				return resultado;
			}
			case T_AND :{
				for(j=0;j<tam_lista_empleados;j++){
					resultado[j] = resultado[j] && lista_salidas[i+1][j];
				}
				break;
			}
			case T_OR :{
				for(j=0;j<tam_lista_empleados;j++){
					resultado[j] = resultado[j] || lista_salidas[i+1][j];
				}
				break;
			}
		}
	}
	return resultado;
}


/*
 * Función que imprime la cabecera de la tabla adaptandose a los diferentes tamaños del campo "puesto" que es el de mayor longitud.
 */
void imprime_cabecera_tabla(int string_mas_largo){
	
	int j = 0;
			
	printf("-------------------------------TABLA EMPLEADOS-------------------------------------\n");
	for(j=0;j<sentencia->n_campos;j++){
		if (string_mas_largo>=22){
			if(charTOcampos(sentencia->lista_campos[j]) == T_ASTERISCO)	
				printf("\tId\tNombre\tPuesto\t\t\t\tAño\tSalario\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_ID)
				printf("\tId\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_NOMBRE)
				printf("\tNombre\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_PUESTO)
				printf("\tPuesto\t\t\t"); 
			if(charTOcampos(sentencia->lista_campos[j]) == T_ANO)
				printf("\tAño\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_SALARIO)
				printf("\tSalario\t");
		}else if (string_mas_largo>=22){
			if(charTOcampos(sentencia->lista_campos[j]) == T_ASTERISCO)	
				printf("\tId\tNombre\tPuesto\t\t\tAño\tSalario\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_ID)
				printf("\tId\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_NOMBRE)
				printf("\tNombre\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_PUESTO)
				printf("\tPuesto\t\t"); 
			if(charTOcampos(sentencia->lista_campos[j]) == T_ANO)
				printf("\tAño\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_SALARIO)
				printf("\tSalario\t");
		}else{
			if(charTOcampos(sentencia->lista_campos[j]) == T_ASTERISCO)	
				printf("\tId\tNombre\tPuesto\t\t\tAño\tSalario\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_ID)
				printf("\tId\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_NOMBRE)
				printf("\tNombre\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_PUESTO)
				printf("\tPuesto\t\t"); 
			if(charTOcampos(sentencia->lista_campos[j]) == T_ANO)
				printf("\tAño\t");
			if(charTOcampos(sentencia->lista_campos[j]) == T_SALARIO)
				printf("\tSalario\t");
		}
	}
	printf("\n");
}


/*
 * Función que recibe el resultado de la consulta e imprime los campos y las lineas de empleados correspondientes.
 + Lista: contiene el resultado final, una cadena de 0 y 1 en función de si la linea de empleado debe ser impresa o no.
 */
void imprime_coincidencias(int * lista){
		
	int i = 0,j = 0;
	int string_mas_largo = 0;
	
	for(i=0;i<tam_lista_empleados;i++)//Escoge el tamaño del campo "puesto" mas largo
		if(strlen(lista_empleados[i].puesto) > string_mas_largo)
			string_mas_largo = strlen(lista_empleados[i].puesto);

	//Función que imprime la cabecera de la tabla
	imprime_cabecera_tabla(string_mas_largo);

	
	for(i=0;i<tam_lista_empleados;i++){ //Recorre la lista de los empleados
		if(lista[i]){ //Comprueba si se imprime los datos del empleado o no
			for(j=0;j<sentencia->n_campos;j++){ //Recorre los campos
				if (strlen(lista_empleados[i].puesto)>=22){
						if(charTOcampos(sentencia->lista_campos[j]) == T_ASTERISCO)
							printf("\t%li\t%s\t%s\t%s\t%.2f\t",lista_empleados[i].idEmpleado,lista_empleados[i].nombre,lista_empleados[i].puesto,lista_empleados[i].anho,lista_empleados[i].salario);
						if(charTOcampos(sentencia->lista_campos[j]) == T_ID)
							printf("\t%li\t",lista_empleados[i].idEmpleado);
						if(charTOcampos(sentencia->lista_campos[j]) == T_NOMBRE)
							printf("\t%s\t",lista_empleados[i].nombre);
						if(charTOcampos(sentencia->lista_campos[j]) == T_PUESTO)
							printf("\t%s",lista_empleados[i].puesto); 
						if(charTOcampos(sentencia->lista_campos[j]) == T_ANO)
							printf("\t%s\t",lista_empleados[i].anho);
						if(charTOcampos(sentencia->lista_campos[j]) == T_SALARIO)
							printf("\t%.2f\t",lista_empleados[i].salario);
				} 
				else if(strlen(lista_empleados[i].puesto)>=14){
						if(charTOcampos(sentencia->lista_campos[j]) == T_ASTERISCO)
							printf("\t%li\t%s\t%s\t\t%s\t%.2f\t",lista_empleados[i].idEmpleado,lista_empleados[i].nombre,lista_empleados[i].puesto,lista_empleados[i].anho,lista_empleados[i].salario);
						if(charTOcampos(sentencia->lista_campos[j]) == T_ID)
							printf("\t%li\t",lista_empleados[i].idEmpleado);
						if(charTOcampos(sentencia->lista_campos[j]) == T_NOMBRE)
							printf("\t%s\t",lista_empleados[i].nombre);
						if(charTOcampos(sentencia->lista_campos[j]) == T_PUESTO)
							printf("\t%s\t",lista_empleados[i].puesto); 
						if(charTOcampos(sentencia->lista_campos[j]) == T_ANO)
							printf("\t%s\t",lista_empleados[i].anho);
						if(charTOcampos(sentencia->lista_campos[j]) == T_SALARIO)
							printf("\t%.2f\t",lista_empleados[i].salario);
				}
				else{
						if(charTOcampos(sentencia->lista_campos[j]) == T_ASTERISCO)
							printf("\t%li\t%s\t%s\t\t\t%s\t%.2f\t",lista_empleados[i].idEmpleado,lista_empleados[i].nombre,lista_empleados[i].puesto,lista_empleados[i].anho,lista_empleados[i].salario);
						if(charTOcampos(sentencia->lista_campos[j]) == T_ID)
							printf("\t%li\t",lista_empleados[i].idEmpleado);
						if(charTOcampos(sentencia->lista_campos[j]) == T_NOMBRE)
							printf("\t%s\t",lista_empleados[i].nombre);
						if(charTOcampos(sentencia->lista_campos[j]) == T_PUESTO)
							printf("\t%s\t\t",lista_empleados[i].puesto); 
						if(charTOcampos(sentencia->lista_campos[j]) == T_ANO)
							printf("\t%s\t",lista_empleados[i].anho);
						if(charTOcampos(sentencia->lista_campos[j]) == T_SALARIO)
							printf("\t%.2f\t",lista_empleados[i].salario);
				}
			}
			printf("\n");
		}
	}
	printf("-----------------------------------------------------------------------------------\n\n\n");		
}



